import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/category_controller.dart';
import '../elements/AddToCartAlertDialog.dart';
import '../elements/CircularLoadingWidget.dart';
import '../elements/DrawerWidget.dart';
import '../elements/FilterWidget.dart';
import '../elements/CategoryGridItemWidget.dart';
import '../elements/SearchBarWidget.dart';
import '../models/route_argument.dart';
import '../repository/user_repository.dart';

class CategoriesWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  CategoriesWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _CategoriesWidgetState createState() => _CategoriesWidgetState();
}

class _CategoriesWidgetState extends StateMVC<CategoriesWidget> {
  // TODO add layout in configuration file
  String layout = 'grid';

  CategoryController _con;

  _CategoriesWidgetState() : super(CategoryController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.listenForCategories();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      drawer: DrawerWidget(),
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => _con.scaffoldKey?.currentState?.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).category,
          style: Theme.of(context)
              .textTheme
              .headline6
              .merge(TextStyle(letterSpacing: 0)),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.filter_alt,
              color: Theme.of(context).hintColor,
            ),
            onPressed: () {
              _con.scaffoldKey?.currentState?.openEndDrawer();
            },
          ),
          // _con.loadCart
          //     ? Padding(
          //         padding: const EdgeInsets.symmetric(
          //             horizontal: 22.5, vertical: 15),
          //         child: SizedBox(
          //           width: 26,
          //           child: CircularProgressIndicator(
          //             strokeWidth: 2.5,
          //           ),
          //         ),
          //       )
          //     : ShoppingCartButtonWidget(
          //         iconColor: Theme.of(context).hintColor,
          //         labelColor: Theme.of(context).accentColor),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _con.refreshCategory,
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: SearchBarWidget(onClickFilter: (filter) {
                  _con.scaffoldKey?.currentState?.openEndDrawer();
                }),
              ),
              SizedBox(height: 10),
              _con.categories.isEmpty
                  ? CircularLoadingWidget(height: 500)
                  : Offstage(
                      offstage: this.layout != 'grid',
                      child: GridView.count(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        primary: false,
                        crossAxisSpacing: 5,
                        mainAxisSpacing: 5,
                        childAspectRatio: (1 / .3),
                        padding: EdgeInsets.symmetric(horizontal: 5),
                        // Create a grid with 2 columns. If you change the scrollDirection to
                        // horizontal, this produces 2 rows.
                        crossAxisCount: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? 2
                            : 4,
                        // Generate 100 widgets that display their index in the List.
                        children:
                            List.generate(_con.categories.length, (index) {
                          return CategoryGridItemWidget(
                              heroTag: 'category_grid',
                              category: _con.categories.elementAt(index),
                              onPressed: () {
                                _con.categories.elementAt(index);
                              });
                        }),
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
