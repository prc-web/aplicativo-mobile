import 'package:flutter/material.dart';

import '../models/category.dart';
import '../models/route_argument.dart';

class CategoryGridItemWidget extends StatefulWidget {
  final String heroTag;
  final Category category;
  final VoidCallback onPressed;

  CategoryGridItemWidget({Key key, this.heroTag, this.category, this.onPressed})
      : super(key: key);

  @override
  _CategoryGridItemWidgetState createState() => _CategoryGridItemWidgetState();
}

class _CategoryGridItemWidgetState extends State<CategoryGridItemWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      highlightColor: Colors.transparent,
      splashColor: Theme.of(context).accentColor.withOpacity(0.08),
      onTap: () {
        Navigator.of(context).pushNamed('/Category',
            arguments: RouteArgument(id: widget.category.id));
      },
      child: Stack(
        alignment: AlignmentDirectional.topEnd,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Hero(
                  tag: widget.heroTag + widget.category.id,
                  child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: NetworkImage(this.widget.category.image.thumb),
                          fit: BoxFit.contain),
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 8),
              Text(
                widget.category.name,
                style: Theme.of(context).textTheme.bodyText2,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 4),
            ],
          ),
        ],
      ),
    );
  }
}
