import 'package:flutter/material.dart';

import '../models/restaurant.dart';
import '../models/route_argument.dart';

class RestaurantGridItemWidget extends StatefulWidget {
  final String heroTag;
  final Restaurant restaurant;
  final VoidCallback onPressed;

  RestaurantGridItemWidget(
      {Key key, this.heroTag, this.restaurant, this.onPressed})
      : super(key: key);

  @override
  _RestaurantGridItemWidgetState createState() =>
      _RestaurantGridItemWidgetState();
}

class _RestaurantGridItemWidgetState extends State<RestaurantGridItemWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      highlightColor: Colors.transparent,
      splashColor: Theme.of(context).accentColor.withOpacity(0.08),
      onTap: () {
        Navigator.of(context).pushNamed('/Details',
            arguments: new RouteArgument(
                heroTag: this.widget.heroTag,
                id: '0',
                param: this.widget.restaurant.id));
      },
      child: Stack(
        alignment: AlignmentDirectional.topEnd,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Hero(
                  tag: widget.heroTag + widget.restaurant.id,
                  child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image:
                              NetworkImage(this.widget.restaurant.image.thumb),
                          fit: BoxFit.cover),
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 5),
              Text(
                widget.restaurant.name,
                style: Theme.of(context).textTheme.bodyText1,
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(height: 2),
              Text(
                widget.restaurant.category.name,
                style: Theme.of(context).textTheme.caption,
                overflow: TextOverflow.ellipsis,
              )
            ],
          ),
        ],
      ),
    );
  }
}
